""" Homework 8
-- Due Sunday, Mar. 22th at 23:59
-- Always write the final code yourself
-- Cite any websites you referenced
-- Use the PEP-8 checker for full style points:
https://pypi.python.org/pypi/pep8
"""

# Your goal will be to create an API for serving basic information
# about Penn courses.
#
# The provided csv file is a few years old
#
# First you will convert a CSV file containing Registrar data to JSON
# format.  Next, you will use this data as a source for serving through
# your API.  We leave this open ended, so please implement helper
# functions.

from flask import Flask


def csv_to_json(csvfile, jsonfile):
    ''' Load the data in csvfile, convert it to a list of dictionaries,
    and then save the result as JSON to jsonfile.

    You can assume that the first line of csvfile will include the field names.
    Each following line should be split into its fields and turned into a
    dictionary. The output should be a list of dictionaries.

    Recall that you did this in previous homework, but you weren't allowed to
    use the DictReader method of the csv module -- now you can. Also, in the
    previous homework, you simply returned the list -- now you should return
    nothing, but write the result in JSON format to the specified file.
    '''
    pass


def load_json(jsonfile):
    ''' Load JSON data from the given filename. Note that this should
    return Python data structures, not a string of JSON.

    If jsonfile does not exist, raise an exception with the message
    "No such file." If jsonfile does not contain valid JSON, raise an
    exception with the message "Invalid JSON."
    '''
    pass


# Convert courses.csv to courses.json.
# Uncomment when you have defined csv_to_json.

#csv_to_json('courses.csv', 'courses.json')

# Load the course data into a global variable, which should be used by
# your Flask application. Uncomment this when you have defined load_json.

#COURSES = load_json('courses.json')

app = Flask(__name__)


@app.route('/')
def home():
    '''This is what you will see if you go to http://127.0.0.1:5000'''
    return 'Hello world.'


@app.route('/courses/<dept>')
@app.route('/courses/<dept>/<code>')
@app.route('/courses/<dept>/<code>/<section>')
def courses(dept, code=None, section=None):
    ''' Returns a list of courses matching the query parameters.

    The response should be JSON of the following format:
    { "results": [list of courses] }
    Each course in the list should be represented by a dictionary.

    Note that JSON should never have lists at the top level due to security
    issues with JavaScript.
    See http://flask.pocoo.org/docs/security/#json-security.
    That's why we return a dictionary with one key/value pair instead.

    For any parameters not provided, match any value for that parameter.
    For instance, accessing /courses/cis should return a list of all CIS
    courses, and accessing courcies/cis/110 should return a list of all
    sections for CIS 110.

    There is also an optional GET request parameter for the 'type' key
    found in the CSV file. You should detect that and use it if it is provided.
    For instance, accessing /courses/cis?type=REC should return a list of all
    CIS recitation sections.
    '''
    pass


@app.route('/schedule', methods=['POST'])
def schedule():
    ''' A POST-only endpoint. Accept POST data of the form:
    {'courses': [ list of courses ]}

    Each course dictionary in the list should have 'dept', 'code', and
    'section' keys. Look up each course and find its meeting times.
    (If the given dictionary does not match any course, just skip it.)
    Construct and return a corresponding JSON schedule, where keys are the
    days of the week, and values are a list of courses that meet on that day.
    See the test case for details.

    Some notes:
    1. Remember that dictionaries are unordered data structures, so the order
    of keys does not matter.
    2. The class lists should be in alphabetical order of class name.
    3. If there is no class on a day, don't include a key for that day.
    '''
    pass


def main():
    app.debug = True
    app.run()


if __name__ == "__main__":
    main()
