import unittest
from hw8 import *
import json
import requests


class TestHomework8(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    # csv_to_json
    def test_csv_to_json(self):
        csv_to_json('simplecsv.csv', 'outjson.json')
        with open('outjson.json', 'rt') as f_name:
            j = json.load(f_name)
        ans = [{'a': '1', 'b': '2'}, {'a': '3', 'b': '4'}]
        self.assertEqual(j, ans)
        with self.assertRaisesRegex(Exception, 'No such file.'):
            csv_to_json('some_trash', 'some_trash')

    # load_json
    def test_load_json(self):
        j = load_json('simplejson.json')
        ans = [['a', 'b', 'c'], {'key': 'val'}]
        self.assertEqual(j, ans)
        with self.assertRaisesRegex(Exception, 'No such file.'):
            load_json('some_trash')
        with self.assertRaisesRegex(Exception, 'Invalid JSON.'):
            load_json('simplecsv.csv')

    # courses
    def test_courses(self):
        # make sure you have your server running
        url = 'http://127.0.0.1:5000'
        r = requests.get('{}/courses/CIS/110/001'.format(url))
        j = r.json()
        ans = {'results': [{'code': '110',
                            'dept': 'CIS',
                            'instructor': 'BROWN B',
                            'name': 'Intro To Comp Prog',
                            'type': 'LEC',
                            'days': 'MWF',
                            'end_time': '11:00',
                            'start_time': '10:00',
                            'location': 'TOWN 100',
                            'section': '001',
                            'times': 'MWF 10-11AM'
                            }]}
        self.assertEqual(j, ans)
        params = {'type': 'LEC'}
        r = requests.get('{}/courses/CIS/110'.format(url), params=params)
        j = r.json()
        self.assertEqual(len(j['results']), 2)

    # schedule
    def test_schedule(self):
        url = 'http://127.0.0.1:5000'
        acct = {'dept': 'ACCT', 'code': '297', 'section': '402'}
        math = {'dept': 'MATH', 'code': '104', 'section': '226'}
        cis = {'dept': 'CIS', 'code': '521', 'section': '001'}
        junk = {'dept': 'ERIC', 'code': '101', 'section': '001'}
        data = {'courses': json.dumps([acct, math, cis, junk])}
        r = requests.post('{}/schedule'.format(url), data=data)
        j = r.json()
        ans = {'Monday': ['Taxes And Bus Strategy: 12:00-1:30'],
               'Tuesday': ['Fundamentals Of Ai: 10:30-12:00'],
               'Wednesday': ['Calculus I: 9:00-10:00',
                             'Taxes And Bus Strategy: 12:00-1:30'],
               'Thursday': ['Fundamentals Of Ai: 10:30-12:00']}
        self.assertEqual(j, ans)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
